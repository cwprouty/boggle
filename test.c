#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int LENGTH = 7;

int recurs(int v) {
	if(v > 0) {
		printf("the value of v is: %d\n", v);
		recurs(--v);
	}
	return 0;
}

// given some dimensions, find adjacent vector locations
int* findAdjValues(int totalsize, int dim, int address) {
	// translate address into x,y coordinates
	int coord[2];
	coord[0] = trunc(address / (totalsize / dim));
	coord[1] = address % (totalsize / dim);
	// since we're dealing with a square, rowmax = colmax
	int rowcolmax = totalsize / dim;
	// we set found = 1 so that we can keep the array length in the first element of the array
	int found = 1;
	int *adj = malloc(sizeof(int) * found);
	for(int i = -1; i < 2; i++) {
		for(int j = -1; j < 2; j++) {
			// if i and j are both not zero, and the tested coordinates are within the outer boundaries of the board, then coordinate to list
			if(!(i == 0 && j == 0) && coord[0] + i >= 0 && coord[0] + i < rowcolmax && coord[1] + j >= 0 && coord[1] + j < rowcolmax) {
				found++;
				adj = realloc(adj, sizeof(int) * found);
				adj[found - 1] = ((coord[0] + i) * dim) + coord[1] + j;
			}	
		}
	}
	// save the length of the vector in the first element
	adj[0] = found;
	return adj; 
}

void printOut(int step, char* msg) {
	for(int i = 0; i < step; i++) {
		printf("-");
	}
	printf("> ");
	printf("%s\n", msg);
}

int contains(int* array, int length, int value) {
	for(int i = 0; i < length; i++) {
		if(array[i] == value) {
			return 1;
		}
	}
	return 0;
}

int pathContains(char* board, char* word, int* paths, int* already, int step, int start) {
	int outcome = 0;
	if(step >= LENGTH) {
		for(int i = 0; i < LENGTH; i++) {
			if(board[already[i]] != word[i]) { return 0; }
		}
		return 1;
	}
	if(paths == NULL) {
		int *a = malloc(sizeof(int) * LENGTH);
		for(int i = 0; i < LENGTH; i++) {
			a[i] = -1;
		}
		a[0] = start;
		int *p = findAdjValues(16, 4, start);
		outcome = outcome || pathContains(board, word, p, a, step + 1, start);	
	}
	else {
		for(int i = 1; i < paths[0]; i++) {
			if(contains(already, LENGTH, paths[i]) == 0) {
					int *a = malloc(sizeof(int) * LENGTH);
					for(int j = 0; j < LENGTH; j++) {
						memcpy(&a[j], &already[j], sizeof(int));
					}
					memcpy(&a[step], &paths[i], sizeof(int)); 
					int *p = findAdjValues(16, 4, paths[i]);
					outcome = outcome || pathContains(board, word, p, a, step + 1, start);
			}
		}
	}
	return outcome;
}


int main(int argc, char* argv[]) {

	int *output = findAdjValues(16, 4, 11);
	for(int i = 1; i < output[0]; i++) {
		printf("%d ", output[i]);
	}
	printf("\n");

	int *things = malloc(sizeof(int) * 5);
	for(int i = 0; i < 5; i++) {
		things[i] = i * 10;
	}

	for(int i = 0; i < 5; i++) {
		printf("%d %p\n", things[i], &things[i]);
	}



/*
	m e t u
	t p l e
	e e a b
	a e e o
*/
	char *l = "metutpleeeabaeeo";
	char *w = "beleapt";
//	LENGTH = 4;
	int ggg = pathContains(l, w, NULL, NULL, 0, 11); 
	printf("OUTPUT: %d\n", ggg);
	return 0;







}
