#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// trims an arbitrary character off the end of a string
char* trimEnd(char* str, char t) {
	// initialize the "found" variable with a null value
	int f = -1;
	for(int i = 0; i < strlen(str); i++) {
		if(str[i] == (int)t && f == -1) {
			// if this is the first desired char we found, note it
			f = i;
		}
		else if(str[i] != (int)t && str[i] != '\0') {
			// if we found a non-desired char that isn't a termination char, reset
			f = -1;
		}
	}
	// if we made it through the string without ending on a desired char, we need to copy whole string
	if(f == -1) f = strlen(str);
	// allocate the size of a string plus one for a termination char
	char* newstr = malloc(sizeof(char) * (f + 1));
	for(int i = 0; i < f; i++) {
		newstr[i] = str[i];
	}
	newstr[f] = '\0';
	return newstr;
}

// returns a boolean indicating whether a specific value is present in a set
int contains(int* superset, int size, int value) {
	for(int i = 0; i < size; i++) {
		if(superset[i] == value) { return 1; }
	}	
	return 0;
}

// returns a boolean indicating whether a subset is represented in a superset
int isSubset(int* superset, int* subset) {
	for(int i = 0; i < 26; i++) {
		if(subset[i] > superset[i]) { return 0; }
	}		
	return 1;
}

// given some dimensions, find adjacent vector locations
int* findAdjValues(int totalsize, int dim, int address) {
	// translate address into x,y coordinates
	int coord[2];
	coord[0] = trunc(address / (totalsize / dim));
	coord[1] = address % (totalsize / dim);
	// since we're dealing with a square, rowmax = colmax
	int rowcolmax = totalsize / dim;
	// we set found = 1 so that we can keep the array length in the first element of the array
	int found = 1;
	int *adj = malloc(sizeof(int) * found);
	for(int i = -1; i < 2; i++) {
		for(int j = -1; j < 2; j++) {
			// if i and j are both not zero, and the tested coordinates are within the outer boundaries of the board, then coordinate to list
			if(!(i == 0 && j == 0) && coord[0] + i >= 0 && coord[0] + i < rowcolmax && coord[1] + j >= 0 && coord[1] + j < rowcolmax) {
				found++;
				adj = realloc(adj, sizeof(int) * found);
				adj[found - 1] = ((coord[0] + i) * dim) + coord[1] + j;
			}	
		}
	}
	// save the length of the vector in the first element
	adj[0] = found;
	return adj; 
}

// return the length of a string as defined by the \0 escape char
int getWordLength(char* word) {
	int len = 0;
	while(word[len] != '\0') {
		len++;
	}
	return len;
}

// accepts a character fulldict and returns a 26-long int array with a count of each letter
int* charsToInts(char* letters) {
	// allocate and initialize an integer fulldict of size 26 to represent the count of each letter
	int *alphabet = malloc(sizeof(int) * 26);
	for(int i = 0; i < 26; i++) { alphabet[i] = 0; }
	// acertain the length of the string passed in
	int s = 0;
	while(letters[s] != '\0') {
		s++;
	}
	// count through the 16 elements of the letters passed into the method, adding to the letter fulldict
	for(int i = 0; i < s; i++) {
		alphabet[(int)letters[i] - 97] += 1;
	}
	return alphabet;
}

// recursive search for all possible paths through the board given some length to search
int pathContains(char* board, char* word, int LENGTH, int* paths, int* already, int step, int start) {
	// begin with the null hypothesis that the word is not found
	int outcome = 0;
	// if we're at the end of the word, determine whether the traversed path is the word we want
	if(step >= LENGTH) {
		for(int i = 0; i < LENGTH; i++) {
			if(board[already[i]] != word[i]) { return 0; }
		}
		// if no differences between path and desired word, return true
		return 1;
	}
	// if this is at the "top" of the recursive search, initialize some stuff
	if(paths == NULL) {
		int *a = malloc(sizeof(int) * LENGTH);
		for(int i = 0; i < LENGTH; i++) {
			a[i] = -1;
		}
		a[0] = start;
		int *p = findAdjValues(16, 4, start);
		outcome = outcome || pathContains(board, word, LENGTH, p, a, step + 1, start);	
		free(a);
		free(p);
	}
	// if this not at the top of the recursive search, recurs through all possible paths, expanding the tree
	else {
		for(int i = 1; i < paths[0]; i++) {
			if(contains(already, LENGTH, paths[i]) == 0) {
					int *a = malloc(sizeof(int) * LENGTH);
					for(int j = 0; j < LENGTH; j++) {
						memcpy(&a[j], &already[j], sizeof(int));
					}
					memcpy(&a[step], &paths[i], sizeof(int)); 
					int *p = findAdjValues(16, 4, paths[i]);
					outcome = outcome || pathContains(board, word, LENGTH, p, a, step + 1, start);
					free(a);
					free(p);
			}
		}
	}
	return outcome;
}

// this method exists to initialize the recursive search and ensure we try each possible starting point
int checkWordPath(char* board, char* word) {
	// acertain the length of the word once
	int len = getWordLength(word);
	if(len < 3) return 0; // words smaller than 3 letters are not allowed in boggle
	for(int i = 0; i < 16; i++) {
		if(word[0] == board[i]) {
			int *path = findAdjValues(16, 4, i);
			int *usedspaces = malloc(sizeof(int) * len);
			for(int j = 0; j < len; j++) {
				usedspaces[j] = -1;
			}
			usedspaces[0] = i;
			if(pathContains(board, word, len, NULL, NULL, 0, i) > 0) return 1;
		}
	}
	return 0;
}

int main(int argc, char* argv[]) {
	// define an fulldict containing the count of each letter found on the board
	int *boggleboard = malloc(sizeof(int) * 26);
	// define the list of letters found on the boggle board
	// char a = 97
	// define the same board in all possible orientation to ensure
	// that the algorithm is rotaionally invariant
//	char *l = "hbnkrsntiviendue";
/*
	u e b o
	t l a e
	e p e e
	m t e a
*/
//	char *l = "uebotlaeepeemtea";
/*
	o e e a
	b a e e
	e l p t
	u t e m
*/
//	char *l = "oeeabaeeelptutem";
/*
	a e t m
	e e p e
	e a l t
	o b e u
*/
//	char *l = "aetmeepeealtobeu";
/*
	m e t u
	t p l e
	e e a b
	a e e o
*/
//	char *l = "metutpleeeabaeeo";
//	char *l = "svderonmcireoafc";
	char *l = "ettnsygemtepaiau";

	// get a count of each letter on the board
	boggleboard = charsToInts(l);

	// define some variables necessary for reading data from the scrabble dictionary file
	FILE *fp;
    char *line = NULL;
    size_t len = 0;
	ssize_t read;
	fp = fopen("words_2.txt", "r");

	// allocate the full dictionary - we'll keep that in memory
	long found = 0;
	char* *fulldict = malloc(sizeof(char*) * found);

	while ((read = getline(&line, &len, fp)) != -1) {
		// we can ignore any words that are greater than 16 characters
		if(strlen(line) < 18) {
				// supposing we've found a word, allocate more memory, trim it, and store it
				found++;
				fulldict = realloc(fulldict, sizeof(char*) * found);
				char *trimmed = trimEnd(line, '\n');
				char *g = malloc(sizeof(char) * strlen(trimmed) + 1);
				memcpy(g, trimmed, sizeof(char) * strlen(trimmed) + 1);
				free(trimmed);
				fulldict[found - 1] = g;
		}
    }
	free(line);
	// close the file we've been working with...
	fclose(fp);

	// we need to build a subdictionary of words to test, similar to full dictionary
	long subcount = 0;
	char* *subdict = malloc(sizeof(char*) * subcount);

	// count through the full dictionary, testing whether each word is a candidate
	// for path testing, based on the count of letters
	for(int i = 0; i < found; i++) {
		// convert string into a letter count
		int *testcase = charsToInts(fulldict[i]);
		// test whether the word could possibly be constructed on the boggle board, ignoring path
		if(isSubset(boggleboard, testcase) == 1) {
			subcount++;
			subdict = realloc(subdict, sizeof(char*) * subcount);
			subdict[subcount - 1] = fulldict[i];
		}
		free(testcase);
	}

	// display the game board to the user
	printf("here's a picture of the board: \n");
	for(int i = 0; i < 4; i++) {
		for(int j = 0; j < 4; j++) {
			printf("%c ", l[(i * 4) + j]);
		}
		printf("\n");
	}

	// print some general statistics about the search space
	printf("total words in dictionary: %ld\n", found);
	printf("of total words, found %ld words to test\n", subcount);
	printf("list of words matching paths on the board:\n");
	long totalpoints = 0;
	// count through all the words identified as possibilities
	for(int i = 0; i < subcount; i++) {
		if(checkWordPath(l, subdict[i]) == 1) {
			printf("%s\n", subdict[i]);
			int wordlen = getWordLength(subdict[i]);
			if(wordlen == 3 || wordlen == 4) {
				totalpoints += 1;
			}
			else if(wordlen == 5) {
				totalpoints += 2;
			}
			else if(wordlen == 6) {
				totalpoints += 3;
			}
			else if(wordlen == 7) {
				totalpoints += 5;
			}
			else if(wordlen >= 8) {
				totalpoints += 11;
			}
		}
	}
	printf("total points earned: %ld\n", totalpoints);

	// free all the memory of elements in the full dictionary
	// this takes care of references in subdict too, so no
	// need for a subdict version
	for(int i = 0; i < found; i++) {
		free(fulldict[i]);
	}

	// free the full dictionary, the sub dictionary, and the board memory
	free(fulldict);
	free(subdict);
	free(boggleboard);

	return 0;
}
